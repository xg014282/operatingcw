#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct my_msgbuf {
    long mtype;
    char mtext[200];
};

void sigusr1_handler(int sig)
{
  write(1, "\nSignal Terminated! Emergency transporter sent!\n", 47);
  printf("\n");
  exit(0);
}

int main()
{
    struct sigaction sa;
    struct my_msgbuf buf;
    int msqid;
    key_t key;
    sa.sa_handler = sigusr1_handler;
    sa.sa_flags = 0; // or SA_RESTART;
    sigemptyset(&sa.sa_mask);

    if (sigaction(SIGUSR1, &sa, NULL)){
      perror("sigaction");
      exit(1);
    }
    if ((key = ftok("kirk3.c", 'B')) == -1) {
        perror("ftok");
        exit(1);
    }
    if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1) {
      perror("msgget error");
      exit(1);
    }
    printf("Process ID: %d\n", getpid());
    printf("Enter lines of text, ^D to quit:\n");

    while(fgets(buf.mtext, sizeof buf.mtext, stdin) != NULL)
    {
      int len = strlen(buf.mtext);
      if(buf.mtext[len - 1] == '\n')
        buf.mtext[len - 1] = '\0';
        if(strncmp(buf.mtext, "URGENT", 6)) {
          printf("Non Urgent Message\n");
          buf.mtype = 1;
        }
        else {
          printf("Urgent Message");
          buf.mtype = 2;
        }
        if(msgsnd(msqid, &buf, len + 1, 0) == -1)
          perror("msgsend Error");
    }
    if(msgctl( msqid, IPC_RMID, NULL) == -1) {
      perror("msgctl Error");
      exit(1);
    }
    return 0;
  }
