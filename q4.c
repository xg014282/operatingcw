#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
int main(){
  //creating the childs
  int child = fork();
  int child2 = fork();

  for (int i = 0; i <= 10; i++) { //loops 10 times
    if (child == 0){
      printf("child process %d counter:  %d\n", getpid(), i);
    } else if (child2 == 0){
      printf("parent process %d counter: %d\n", getppid(), i );

    }
    sleep(1); //1 second delay
    }
return 0;
}
