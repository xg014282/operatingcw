#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>

sem_t mutex ;//semaphore

#define NUM_THREADS 5     //number of threads used

float total=0;    /* Set global variable */

void *compute(void *threadid)     /* compute function just does something. */
{
    int i;
    float result=0;

    /* for a large number of times */
    for(i=0;i<2000000000;i++)
      result = sqrt(1000.0) * sqrt(1000.0);
    sem_post(&mutex); // increment semaphore value
    /* Print the result - should be no surprise */
    printf("Result is %f\n",result);

    /* to keep a running total in the global variable total */
    int sem_value;
    sem_getvalue(&mutex, &sem_value);//gets value of semaphore
    total =  result * sem_value;


    /* Print running total so far. */
    printf("Total is %f\n",total);

    pthread_exit(NULL);
} // code to be continued

int main()
{
    pthread_t threads[NUM_THREADS]; //thread array

    long t;

    //set up semphores
    printf("\n");
    sem_init(&mutex, 0, 0);//intilaise the semaphore with value 0
    /* to loop and create the required number of processes */
    /* NOTE carefully how only the child process is left to run */
    for(t=0;t<NUM_THREADS;t++) // loops 5 times since the number of threads is 5
    {
          /* give a message about the thread ID */
            printf("IN MAIN: creating thread %ld\n", t);

            /* call the function to do some computation */
            pthread_create(&threads[t], NULL, compute, (void *)t); // creates a thread in position t of array
            pthread_join(threads[t],NULL);// join the threads


    }

    pthread_exit(NULL);
    sem_destroy(&mutex);//destroys the semaphore
 }
